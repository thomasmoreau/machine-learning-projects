# Machine Learning Projects

- **Covid-19**: Etude d'un modèle permettant de prédire si une personne est infectée par le virus en fonction de données médicales. Classification sur la classe binaire "SARS-cov 2 test" avec SVM (svc) et Adaboost. Recall=0.75, F1_score= 0.58
